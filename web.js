//var connect = require('connect');
//var http = require('http');
//var serveStatic = require('serve-static')
//
//var app = connect();
//
//app.use(serveStatic("" + __dirname + "/dist"));
//
////create node.js http server and listen on port
//http.createServer(app).listen(process.env.PORT || 5000);

var restify = require('restify');

function respond(req, res, next) {
  res.send('hello ' + req.params.name);
  next();
}

var server = restify.createServer();
server.get('/hello/:name', respond);
server.head('/hello/:name', respond);

server.get(/\/?.*/, restify.serveStatic({
  directory: "" + __dirname + "/dist",
  default: 'index.html'
}));

server.listen(process.env.PORT || 5000, function () {
  console.log('%s listening at %s', server.name, server.url);
});
