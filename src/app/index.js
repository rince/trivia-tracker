'use strict';

angular.module('triviaTracker', [
  'ngAnimate',
  'ngCookies',
  'ngTouch',
  'ngSanitize',
  'ui.router',
  'mgcrea.ngStrap',
  'LocalForageModule'
])
  .config(function ($stateProvider, $urlRouterProvider, $localForageProvider) {
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'app/main/main.html',
        controller: 'MainCtrl'
      })
      .state('new-game', {
        url: '/new-game',
        templateUrl: 'app/game/new-game.html',
        controller: 'NewGameCtrl'
      })
      .state('games-list', {})
      .state('game', {
        url: '/game/:gameId',
        templateUrl: 'app/game/game.html',
        controller: 'GameCtrl'
      })
      .state('round', {
        url: '/game/:gameId/round/:roundId',
        templateUrl: 'app/round/round.html',
        controller: 'RoundCtrl',
        resolve: {
          round: function (GameService, $stateParams) {
            return GameService.getRound($stateParams.gameId, $stateParams.roundId);
          }
        }
      });

    $urlRouterProvider.otherwise('/');

    $localForageProvider.config({
      name: 'triviaTracker', // name of the database and prefix for your data, it is "lf" by default
      storeName: 'trivia', // name of the table
      description: 'Storage'
    });

  })
  .run(function ($rootScope) {
    //$rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
    //  console.log("State change start " + fromState.name + " to " + toState.name);
    //});
    //
    //$rootScope.$on('$stateNotFound', function (event, unfoundState, fromState, fromParams) {
    //  console.log(unfoundState.to); // "lazy.state"
    //  console.log(unfoundState.toParams); // {a:1, b:2}
    //  console.log(unfoundState.options); // {inherit:false} + default options
    //});
    //
    //$rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
    //  console.log("State change success " + fromState.name + " to " + toState.name);
    //});
    //
    //$rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams, error) {
    //  console.log("error changing state " + error);
    //});
  })
;
