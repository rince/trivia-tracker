'use strict';

angular.module('triviaTracker')
  .controller('MainCtrl', function ($scope, GameService) {

    function listGames() {
      GameService.listGames()
        .then(function (games) {
          $scope.games = games;
        });
    }

    listGames();


    $scope.clear = function () {
      GameService.clearAll()
        .then(function () {
          listGames();
        });
    }
  });
