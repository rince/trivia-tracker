'use strict';

angular.module('triviaTracker')
  .controller('GameCtrl', function ($scope, GameService, $state, $stateParams) {


    GameService.getGame($stateParams.gameId)
      .then(function (game) {
        $scope.game = game;
      });

    GameService.getRounds($stateParams.gameId)
      .then(function (rounds) {
        $scope.rounds = rounds;
      });

    GameService.gameScore($stateParams.gameId)
      .then(function(score){
        $scope.gameScore = score.gameScore;
      });

    $scope.points = function (round) {
      return GameService.roundScore(round);
    };

    $scope.goToRound = function(index) {
      $state.go('round', {gameId: $stateParams.gameId, roundId: index + 1})
    };

    $scope.deleteGame = function(){
      GameService.deleteGame($stateParams.gameId)
        .then(function(){
          $state.go('home');
        })
    };
  });
