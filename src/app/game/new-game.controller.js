'use strict';

angular.module('triviaTracker')
  .controller('NewGameCtrl', function ($scope, GameService, $state) {

    $scope.game = {
      date: Date.now(),
      rounds: 8,
      venue: 'The Highball'
    };


    $scope.saveGame = function () {
      GameService.saveNewGame($scope.game)
        .then(function (result) {
          $state.go('round', {gameId: result.gameId, roundId: 1});
        });
    }

  });
