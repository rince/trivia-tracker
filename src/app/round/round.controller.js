'use strict';

angular.module('triviaTracker')
  .controller('RoundCtrl', function (round, $scope, GameService, $state, $stateParams) {

    $scope.round = round;

    $scope.roundNumber = parseInt($stateParams.roundId);

    var gamePromise = GameService.getGame($stateParams.gameId);

    $scope.$watch('round.numberOfPoints', function (newLength, oldLength) {
      if (newLength !== oldLength) {
        $scope.round.points = new Array(newLength);
      }
      //var currentLength = $scope.round.points.length;
      //if (newLength < currentLength) {
      //  $scope.round.points.splice(-1, (currentLength - newLength));
      //} else if (newLength > currentLength) {
      //  $scope.round.points = $scope.round.points.concat(new Array(newLength - currentLength));
      //}
    });

    //autosave
    //todo: maybe clean up toggleJoker?
    $scope.$watch('round', function () {
      saveRound()
        .finally(function() {
          scores();
        });
    }, true);

    $scope.correct = function () {
      if ($scope.round.correct !== $scope.round.numberOfPoints) {
        $scope.round.correct++;
      }
    };

    $scope.incorrect = function () {
      if ($scope.round.correct !== 0) {
        $scope.round.correct--;
      }
    };

    $scope.nextRound = function () {
      gamePromise.then(function (game) {
          if ($scope.roundNumber + 1 > game.rounds) {
            //navigate to game
            $scope.goToGame();
          } else {
            saveRound();
            $state.go('round', {gameId: $stateParams.gameId, roundId: $scope.roundNumber + 1});
          }
        }
      )
    };

    $scope.goToGame = function () {
      $state.go('game', {gameId: $stateParams.gameId});
    };

    $scope.previousRound = function () {
      if ($scope.roundNumber - 1 === 0) {
        //navigate to game
        $scope.goToGame();
      } else {
        saveRound();
        $state.go('round', {gameId: $stateParams.gameId, roundId: $scope.roundNumber - 1});
      }
    };

    $scope.toggleJoker = function () {
      var jokerPromise;
      if ($scope.round.joker) {
        $scope.round.joker = false;
        //rely on watch to save the round
      } else {
        jokerPromise = GameService.setJoker($stateParams.gameId, $stateParams.roundId)
        jokerPromise.then(function () {
          return GameService.getRound($stateParams.gameId, $stateParams.roundId)
        })
          .then(function (round) {
            $scope.round = round;
            scores();
          });
      }

    };

    function scores() {
      $scope.roundScore = GameService.roundScore($scope.round);
      gameScore();
    }

    function saveRound() {
      return GameService.saveRound($stateParams.gameId, $stateParams.roundId, $scope.round);
    }

    function gameScore() {
      GameService.gameScore($stateParams.gameId, $stateParams.roundId)
        .then(function (result) {
          $scope.gameScore = result.gameScore;
          $scope.maxScore = result.maxScore;
          $scope.maxScoreJoker = result.maxScoreJoker;
          $scope.maxScoreBestJoker = result.maxScoreBestJoker;
        });
    }


  }
)
;
