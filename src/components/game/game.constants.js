'use strict';

angular.module('triviaTracker')
  .constant('PREFIX', {
    game: 'G-',
    round: 'R-'
  })
  .constant('SUFFIX', {
    round: '-'
  });
