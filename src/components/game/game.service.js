'use strict';

angular.module('triviaTracker')
  .factory('GameService', function ($localForage, PREFIX, SUFFIX, $q) {


    function generateRandomIdentifier() {
      var id = Math.random().toString(36).slice(2);
      return id.length === 16 ? id : generateRandomIdentifier();
    }

    function roundKey(gameId, roundId) {
      return PREFIX.round + roundId + SUFFIX.round + gameId;
    }

    function calculateRoundScore(round) {
      return round.joker ? round.correct * 2 : round.correct;
    }

    function getAllRounds(gameId) {
      return $localForage.getItem(gameId)
        .then(function (game) {
          var numberOfRounds = game.rounds;

          var roundPromises = [];
          for (var i = 1; i <= numberOfRounds; i++) {
            roundPromises.push($localForage.getItem(roundKey(gameId, i)));
          }

          return $q.all(roundPromises);
        });
    }

    function saveRound(gameId, roundId, round) {
      return $localForage.setItem(roundKey(gameId, roundId), round);
    }

    function getGame(gameId) {
      return $localForage.getItem(gameId)
        .then(function (game) {
          return angular.extend(game, {gameId: gameId});
        });
    }

    function getRound(gameId, roundId) {
      var roundNumber = parseInt(roundId);
      var defaultRound = {
        joker: false,
        numberOfPoints: roundNumber === 2 || roundNumber === 8 ? 16 : 8,
        correct: 0
      };

      return $localForage.getItem(roundKey(gameId, roundId))
        .then(function (round) {
          if (angular.isUndefined(round)) {
            return defaultRound;
          } else {
            return round;
          }
        })
    }

    return {
      saveNewGame: function (game) {
        var id = PREFIX.game + game.venue.toLowerCase().replace(/\W+/g, '') + moment(game.date).format('MMDDYYYY');
        //TODO: ensure no duplicates
        return $localForage.setItem(id, game)
          .then(function (data) {
            return {gameId: id, game: data};
          });
      },
      listGames: function () {
        return $localForage.keys().
          then(function (keys) {
            var gameIds = [];
            angular.forEach(keys, function (key) {
              if (_.startsWith(key, PREFIX.game)) {
                gameIds.push(key);
              }
            });

            return gameIds;
          })
          .then(function (gameIds) {
            var promises = [];
            angular.forEach(gameIds, function (gameId) {
              promises.push(getGame(gameId));
            });
            return $q.all(promises);
          });

      },
      getGame: function (gameId) {
        return getGame(gameId);
      },
      deleteGame: function(gameId) {
        return $localForage.getItem(gameId)
          .then(function (game) {
            var numberOfRounds = game.rounds;

            var toDelete = [gameId];
            for (var i = 1; i <= numberOfRounds; i++) {
              toDelete.push(roundKey(gameId, i));
            }

            return $localForage.removeItem(toDelete);
          });
      },
      getRounds: function (gameId) {
        return getAllRounds(gameId);
      },
      getRound: function (gameId, roundId) {
        return getRound(gameId, roundId);
      },
      saveRound: function (gameId, roundId, round) {
        return saveRound(gameId, roundId, round);
      },
      clearJoker: function (gameId, roundId) {
        return getRound(gameId, roundId)
          .then(function (round) {
            round.joker = false;
            return saveRound(gameId, roundId, round);
          })
      },
      setJoker: function (gameId, roundId) {
        return getAllRounds(gameId)
          .then(function (allRounds) {
            var jokerRound = parseInt(roundId);
            var savePromises = [];
            for (var i = 0; i < allRounds.length; i++) {
              var round = allRounds[i];
              if (!angular.isUndefined(round)) {
                if (i + 1 === jokerRound) {
                  round.joker = true;
                } else {
                  round.joker = false;
                }
                savePromises.push(saveRound(gameId, i + 1, round));
              }
            }
            return $q.all(savePromises);
          });
      },
      gameScore: function (gameId, roundId) {
        return getAllRounds(gameId)
          .then(function (allRounds) {
            var currentRound = parseInt(roundId);

            var gameScore = 0;
            var bestRound = -1;
            var bestRoundScore = -1;
            var bestGameScore = 0;

            var maxScore = 0;
            var maxScoreJoker = 0;

            for (var i = 0; i < allRounds.length; i++) {
              var round = allRounds[i];
              if (!angular.isUndefined(round)) {
                var roundScore = calculateRoundScore(round);
                gameScore += roundScore;
                if (gameScore > bestRoundScore) {
                  bestRound = i + 1;
                  bestRoundScore = roundScore;
                }
                if (i < currentRound) {
                  maxScore += round.numberOfPoints;
                }
              }
            }

            //calculate optimum joker score
            bestGameScore = (maxScore - bestRoundScore) + (bestRoundScore * 2);


            return {
              gameScore: gameScore,
              bestGameScore: bestGameScore,
              maxScore: maxScore,
              maxScoreJoker: maxScoreJoker,
              bestRound: bestRound
            };
          });
      },
      roundScore: function (round) {
        return calculateRoundScore(round);
      },
      clearAll: function () {
        return $localForage.clear();
      }
    }
  });
